<?php
/**
 * Message translations.
 *
 * This file is automatically generated by 'yii message/extract' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
return [
    'About' => 'Описание',
    'All statuses' => 'Все состояния',
    'Are you sure you want to delete this item?' => 'Вы уверены, что хотите удалить эту запись?',
    'Cancel' => 'Отменить',
    'Contact' => 'Связаться',
//    'Create Status' => 'Создание состояния',
    'Delete' => 'Удалить',
    'Dmitry Dobryshin' => 'Добрышин Дмитрий',
    'Home' => 'Начало',
    'Identifier' => 'Идентификатор (неизменяемый)',
    'Identifire' => 'Идентификатор',
    'Import' => 'Импортировать',
    'Incorrect username or password.' => 'Неправильное имя пользователя или пароль',
    'Inventory' => 'Инвентаризация',
    'Items' => 'Оборудование',
    'Login' => 'Вход',
    'Logout' => 'Выход',
    'Number of equipment by regions' => 'Количество оборудования по подразделениям',
    'Number of items by type' => 'Количество оборудования по типам',
    'or' => 'или',
    'Password' => 'Пароль',
    'Please contact us if you think this is a server error. Thank you.' => 'Свяжитесь с нами, если вы считаете, что это ошибка сервера. Спасибо.',
    'Please fill out the following fields to login:' => 'Пожалуйста, заполните следующие поля для входа:',
    'Register' => 'Учесть',
    'Remember Me' => 'Запомнить меня',
    'Reset' => 'Сброс',
    'Save' => 'Сохранить',
    'Search' => 'Поиск',
//    'Statuses' => 'Состояния',
    'The above error occurred while the Web server was processing your request.' => 'Эта ошибка произошла во время обработки вашего запроса веб-сервером.',
    'The requested page does not exist.' => 'Запрошенная страница отсутствует.',
    'To modify the username/password, please check out the code' => 'Чтобы изменить имя пользователя/пароль, пожалуйста, посмотрите код',
    'Update' => 'Изменение',
    'Update Status: {name}' => 'Изменение состояния: {name}',
    'Username' => 'Имя пользователя',
    'You may login with' => 'Вы можете войти как',
//    'Types' => 'Типы',
//    'Состояние' => 'Состояние',
//    'Insert' => '@@Добавить@@',
//    'Status' => '@@Состояние@@',
//    'View' => '@@Просмотреть@@',
];
